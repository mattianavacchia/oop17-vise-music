package org.vise.view;

/**
 * 
 * Interface that represent the operations that the rlform has to performe.
 *
 */
public interface UIRLForm {

    /**
     * Display the Player after the login.
     */
    void goToPlayerAfterLogin();
}
